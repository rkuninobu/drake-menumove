Pod::Spec.new do |s|
  s.name         = "WebAPI"
  s.version      = "0.1.6"
  s.summary      = "Scroll view like Star Wars opening crawl."
  s.license      = { :type => 'MIT', :file => 'LICENSE.txt' }
  s.homepage     = "https://github.com/somtd/SWScrollView"
  s.author       = { "SOMTD" => "matsuda-so[at]kayac.com" }
  s.source       = { :git => "https://rkuninobu@bitbucket.org/drakenetworks/webapi.git", :tag => "0.1.6" }
  s.platform     = :ios, '6.0'
  s.requires_arc = true
  s.source_files = 'WebAPI/WebAPI/api/*'
  s.dependency 'Evernote-SDK-iOS'
  s.dependency 'Dropbox-Sync-API-SDK'
  s.dependency 'AFNetworking'
  s.dependency 'MKNetworkKit'
  s.dependency 'SVProgressHUD'
  s.xcconfig  =  { 'FRAMEWORK_SEARCH_PATHS' => '"$(PODS_ROOT)/Dropbox-Sync-API-SDK/dropbox-ios-sync-sdk-2.0.3"' }
end