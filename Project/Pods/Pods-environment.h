
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// ButtonMove
#define COCOAPODS_POD_AVAILABLE_ButtonMove
#define COCOAPODS_VERSION_MAJOR_ButtonMove 0
#define COCOAPODS_VERSION_MINOR_ButtonMove 1
#define COCOAPODS_VERSION_PATCH_ButtonMove 0

// Reveal-iOS-SDK
#define COCOAPODS_POD_AVAILABLE_Reveal_iOS_SDK
#define COCOAPODS_VERSION_MAJOR_Reveal_iOS_SDK 1
#define COCOAPODS_VERSION_MINOR_Reveal_iOS_SDK 0
#define COCOAPODS_VERSION_PATCH_Reveal_iOS_SDK 3

