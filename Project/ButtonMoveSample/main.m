//
//  main.m
//  ButtonMoveSample
//
//  Created by 國信隆之介 on 2014/02/15.
//  Copyright (c) 2014年 國信隆之介. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
