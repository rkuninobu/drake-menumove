//
//  ViewController.m
//  ButtonMoveSample
//
//  Created by 國信隆之介 on 2014/02/15.
//  Copyright (c) 2014年 國信隆之介. All rights reserved.
//

#import "ViewController.h"
#import "ButtonMenu.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    ButtonMenu * button = [[ButtonMenu alloc] init];
    button.delegate = self;
    [self.view addSubview:button];
    button.openphoto = @"nami.jpg";
    button.closephoto = @"syan.jpg";
    button.rotateflag = YES;
    [button initAllMenu:@[@"cro.jpg",@"cro.jpg",@"cro.jpg",@"cro.jpg"] frame:CGRectMake(250, 100, 50, 80) space:0 pointEnd:10];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)pushMenButton:(ButtonMenu*)view settag:(int)tag{
}
 
@end
