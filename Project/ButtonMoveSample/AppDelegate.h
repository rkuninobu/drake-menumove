//
//  AppDelegate.h
//  ButtonMoveSample
//
//  Created by 國信隆之介 on 2014/02/15.
//  Copyright (c) 2014年 國信隆之介. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
