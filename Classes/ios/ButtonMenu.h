//
//  ButtonMenu.h
//  Pods
//
//  Created by 國信隆之介 on 2014/02/16.
//
//

#import <UIKit/UIKit.h>


@interface ButtonMenu : UIView

+ (ButtonMenu*)sharedManager;
@property (nonatomic,strong) NSArray * buttonmenu;
@property (nonatomic,strong) UIButton * topbutton;
@property (nonatomic,assign) id delegate;
@property (nonatomic,strong) NSString * closephoto;
@property (nonatomic,strong) NSString * openphoto;
@property (nonatomic) BOOL rotateflag;

-(void)initAllMenu:(NSArray *)aMenusArray frame:(CGRect)frame space:(CGFloat)space pointEnd:(CGFloat)pointEnd;
-(void)pushTopMenu:(UIButton*)button;
-(void)changeAllbuttonhidden:(BOOL)ishidden;
@end

@protocol ButtonMenu
//個々のボタンを押した時のデリゲート
- (void)pushMenButton:(ButtonMenu*)view settag:(int)tag;
//トップのボタンを押した時のデリゲート
- (void)pushTopButton:(ButtonMenu*)view;
@end