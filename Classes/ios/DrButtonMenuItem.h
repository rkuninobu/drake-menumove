//
//  DrButtonMenuItem.h
//  Pods
//
//  Created by 國信隆之介 on 2014/02/16.
//
//

#import <UIKit/UIKit.h>

@interface DrButtonMenuItem : UIButton

@property (nonatomic, assign) float nearPoint;
@property (nonatomic, assign) float endPoint;
@property (nonatomic, assign) float farPoint;
@property (nonatomic, assign) CGPoint startPoint;

@end
