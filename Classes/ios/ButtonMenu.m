//
//  ButtonMenu.m
//  Pods
//
//  Created by 國信隆之介 on 2014/02/16.
//
//

#import "ButtonMenu.h"
#import "DrButtonMenuItem.h"

@implementation ButtonMenu{
    int flag;
}

static ButtonMenu *  _sharedInstance = nil;

+ (ButtonMenu*)sharedManager
{
    // インスタンスを作成する
    if (!_sharedInstance) {
        _sharedInstance = [[ButtonMenu alloc] init];
    }
    
    return _sharedInstance;

}

/*****************
 *アニメーションをするための下準備のメソッド。ボタンとボタンが動く位置を規定する。
 *@param aMenuArray menuボタンの画像名が入った配列
 *@param frame menuボタンの大きさと位置を指定
 *****************/
-(void)initAllMenu:(NSArray *)aMenusArray frame:(CGRect)frame space:(CGFloat)space pointEnd:(CGFloat)pointEnd{
    flag = 0;
    
    self.frame = frame;
    self.frame = CGRectMake(0, frame.origin.y, self.superview.frame.size.width, frame.size.height);
    
    //ボタンの初期位置の設定
    CGRect btninitrect = CGRectMake(frame.origin.x, 0, frame.size.width, frame.size.height);
    NSMutableArray * buttonarray = [NSMutableArray array];
    
    for (int num = 0; num < aMenusArray.count;num++) {
        //移動するボタン作成
        DrButtonMenuItem * button = [[DrButtonMenuItem alloc] initWithFrame:btninitrect];
        [self addSubview:button];
        [button setBackgroundImage:[UIImage imageNamed:aMenusArray[num]] forState:UIControlStateNormal];
        //ボタンのスタート位置、終了位置、バウンドさせるための終了位置より先、手前の位置指定。
        button.startPoint = self.center;
        float endlength = button.frame.size.width/2-(button.frame.origin.x - pointEnd)/(aMenusArray.count*2+1)*(num+1)*2 -(pointEnd - space)
        ;
        button.endPoint = -endlength;
        button.nearPoint = -endlength-10;
        button.farPoint = -endlength + 10;
        button.tag = num+1;
        button.contentMode = UIViewContentModeScaleToFill;
        [button addTarget:self action:@selector(startMenuAction:) forControlEvents:UIControlEventTouchUpInside];
        [buttonarray addObject:button];
    }
    self.buttonmenu = [[NSArray alloc] initWithArray:buttonarray];
    self.topbutton = [[UIButton alloc] initWithFrame:btninitrect];
    [self.topbutton setImage:[UIImage imageNamed:self.openphoto] forState:UIControlStateNormal];
    //[self.topbutton setImage:[UIImage imageNamed:@"btn_left.png"] forState:UIControlStateNormal];
    [self.topbutton addTarget:self action:@selector(pushTopMenu:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.topbutton];
    //self.topbutton.center = [self.topbutton convertPoint:self.center fromView:self];
}

#pragma mark - util
/**
 * メニューボタンの非表示と表示を操作するメソッド
 *@param hidden 非表示かどうか。YESで非表示。NOで表示。
 */
-(void)changeAllbuttonhidden:(BOOL)hidden{
    self.topbutton.hidden = hidden;
    for (DrButtonMenuItem * item in self.buttonmenu) {
        item.hidden = hidden;
    }
}

#pragma mark - ボタンを押した時の処理
/*****************
 *アニメーションボタンを押した時の処理。閉じている時は開く。開いている時は閉じるアニメーションが走る。
 *@param button topbutton
 *****************/
-(void)pushTopMenu:(UIButton*)button{
   
    
    if(flag == 0){
        //開く処理
        [self.topbutton setImage:[UIImage imageNamed:self.closephoto] forState:UIControlStateNormal];
        for (DrButtonMenuItem * item in self.buttonmenu) {
            CAAnimationGroup * anime = [self _expand:item setrotate:self.rotateflag];
            [item.layer addAnimation:anime forKey:@"anime"];
            item.center = CGPointMake(item.endPoint, self.topbutton.center.y);
        }
    }else if (flag == 1){
         //閉じる処理
        [self.topbutton setImage:[UIImage imageNamed:self.openphoto] forState:UIControlStateNormal];
        for (DrButtonMenuItem * item in self.buttonmenu) {
            
            CAAnimationGroup * anime = [self _close:item setrotate:self.rotateflag];
            [item.layer addAnimation:anime forKey:@"anime"];
            item.center = CGPointMake(self.topbutton.center.x, self.topbutton.center.y);
        }

    }
    
    if ([_delegate respondsToSelector:@selector(pushTopButton:)]) {
        [_delegate pushTopButton:self];
    }
    
}

/*****************
 *アニメーションで開いたボタンを押した時の処理。tagをdelegateで返す。
 *@param button 開いたボタン
 *****************/
-(void)startMenuAction:(UIButton*)button{
    if ([_delegate respondsToSelector:@selector(pushMenButton:settag:)]) {
        [_delegate pushMenButton:self settag:(int)button.tag];
    }
    
}
#pragma mark - アニメーションの処理
/*****************
 *開くアニメーション。回転と移動を同時に行う。
 *@param item アニメーションさせるボタン。どこまでアニメーションさせるか規定する。
 *@param isrotate 回転させるかどうかを決めるフラグ
 *@return CAAnimationGroup 回転と移動をまとめたアニメーショングループ
 *****************/
- (CAAnimationGroup *)_expand:(DrButtonMenuItem*)item setrotate:(BOOL)isrotate
{
	
    CAKeyframeAnimation *rotateAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotateAnimation.values = [NSArray arrayWithObjects:[NSNumber numberWithFloat:M_PI],[NSNumber numberWithFloat:0.0f], nil];
    rotateAnimation.duration = 0.5f;
    rotateAnimation.keyTimes = [NSArray arrayWithObjects:
                                [NSNumber numberWithFloat:.3],
                                [NSNumber numberWithFloat:.4], nil];
    
    CAKeyframeAnimation *positionAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    positionAnimation.duration = 0.5f;
    
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, NULL, item.center.x,self.topbutton.center.y);
    CGPathAddLineToPoint(path, NULL, item.farPoint, self.topbutton.center.y);
    CGPathAddLineToPoint(path, NULL, item.nearPoint, self.topbutton.center.y);
    CGPathAddLineToPoint(path, NULL, item.endPoint, self.topbutton.center.y);
    positionAnimation.path = path;
    CGPathRelease(path);
    
    CAAnimationGroup *animationgroup = [CAAnimationGroup animation];
    NSMutableArray * animaarray = [NSMutableArray array];
    [animaarray addObject:positionAnimation];
    if (isrotate) {
        [animaarray addObject:rotateAnimation];
    }
    animationgroup.animations = animaarray;
    animationgroup.duration = 0.5f;
    animationgroup.fillMode = kCAFillModeForwards;
    animationgroup.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    animationgroup.removedOnCompletion = NO;
    
    flag = 1;

    return animationgroup;
}

/*****************
 *閉じるアニメーション。回転と移動を同時に行う。
 *@param item アニメーションさせるボタン。どこまでアニメーションさせるか規定する。
 *@param isrotate 回転させるかどうかを決めるフラグ
 *@return CAAnimationGroup 回転と移動をまとめたアニメーショングループ
 *****************/
- (CAAnimationGroup *)_close:(DrButtonMenuItem*)item setrotate:(BOOL)isrotate
{
    
    CAKeyframeAnimation *rotateAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotateAnimation.values = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0.0f],[NSNumber numberWithFloat:M_PI],[NSNumber numberWithFloat:0.0f], nil];
    rotateAnimation.duration = 0.5;
    rotateAnimation.keyTimes = [NSArray arrayWithObjects:
                                [NSNumber numberWithFloat:.0],
                                [NSNumber numberWithFloat:.4],
                                [NSNumber numberWithFloat:.5], nil];
    
    CAKeyframeAnimation *positionAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    positionAnimation.duration = 0.5;
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, NULL, item.endPoint, self.topbutton.center.y);
    CGPathAddLineToPoint(path, NULL, self.topbutton.center.x + 10, self.topbutton.center.y);
    CGPathAddLineToPoint(path, NULL, self.topbutton.center.x, self.topbutton.center.y);
    
    positionAnimation.path = path;
    CGPathRelease(path);
    
    CAAnimationGroup *animationgroup = [CAAnimationGroup animation];
    NSMutableArray * animaarray = [NSMutableArray array];
    [animaarray addObject:positionAnimation];
    if (isrotate) {
        [animaarray addObject:rotateAnimation];
    }
    animationgroup.animations = animaarray;
    animationgroup.duration = 0.5f;
    animationgroup.fillMode = kCAFillModeForwards;
    animationgroup.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    //animationgroup.delegate = self;
    
    /*
    if(_flag == 0){
        [animationgroup setValue:@"lastAnimation" forKey:@"id"];
    }
    
    [item.layer addAnimation:animationgroup forKey:@"Close"];
    item.center = item.startPoint;
    
    _flag --;
     
     */
    animationgroup.removedOnCompletion = NO;
    
    flag =0;
    
    return animationgroup;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
