# ButtonMove

[![Version](http://cocoapod-badges.herokuapp.com/v/ButtonMove/badge.png)](http://cocoadocs.org/docsets/ButtonMove)
[![Platform](http://cocoapod-badges.herokuapp.com/p/ButtonMove/badge.png)](http://cocoadocs.org/docsets/ButtonMove)

## Usage

To run the example project; clone the repo, and run `pod install` from the Project directory first.

## Requirements

## Installation

ButtonMove is available through [CocoaPods](http://cocoapods.org), to install
it simply add the following line to your Podfile:

    pod "ButtonMove"

## Author

國信, jama249@gmail.com

## License

ButtonMove is available under the MIT license. See the LICENSE file for more info.

